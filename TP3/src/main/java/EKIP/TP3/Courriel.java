package EKIP.TP3;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Courriel {

	//------- ATTRIBUTS --------
	
	private String adresseDest;
	private String titre;
	private String corps;
	private ArrayList<String> piecesJointes;
	
	
	//------ CONSTRUCTEURS ------
	
	public Courriel() {
		
		this.adresseDest = "nom.exemple@gmail.com";
		this.titre = "";
		this.corps = "";
		this.piecesJointes = new ArrayList<String>();
	}
	
	public Courriel(String adresseDest, String titre, String corps) {
		
		this.adresseDest = adresseDest;
		this.titre = titre;
		this.corps = corps;
		this.piecesJointes = new ArrayList<String>();
	}

	
	
	//----- ACCESSEURS EN LECTURE ET ECRITURE --------
	
	public String getAdresseDest() {
		
		return adresseDest;
	}

	public void setAdresseDest(String adresseDest) {
		
		this.adresseDest = adresseDest;
	}

	public String getTitre() {
		
		return titre;
	}

	public void setTitre(String titre) {
		
		this.titre = titre;
	}

	public String getCorps() {
		
		return corps;
	}

	public void setCorps(String corps) {
		
		this.corps = corps;
	}

	public ArrayList<String> getPiecesJointes() {
		
		return piecesJointes;
	}

	public void setPiecesJointes(ArrayList<String> piecesJointes) {
		
		this.piecesJointes = piecesJointes;
	}
	
	// ------- METHODES ---------
	
	public void ajouterPieceJointe(String piece){
		
		this.piecesJointes.add(piece);
	}
	
	public void envoyer() throws CourrierInvalideException {
		
		//on verifie si les conditions pour au'un courrier soit valide sont respectées
		//on renvoie une exception sinon
		
		if(!this.adresseDest.matches("^[A-Za-z][A-Za-z0-9]+@[A-Za-z]+\\.[A-Za-z]+$")) throw new CourrierInvalideException("ERREUR: adresse incorrecte");
		if(this.titre.isEmpty() || this.titre == null) throw new CourrierInvalideException("ERREUR: titre vide");
		if(this.corps.isEmpty() || (!this.corps.contains("PJ") && !this.corps.contains("joint") && !this.corps.contains("jointe"))) throw new CourrierInvalideException("ERREUR: mots manquants dans le corps");
		if(this.piecesJointes.isEmpty()) throw new CourrierInvalideException("ERREUR: pas de pieces jointes");
	
		//si tout est conforme on envoie le mail
		System.out.println("Mail envoyé");
	}
	
}

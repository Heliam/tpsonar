package EKIP.TP3;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;


public class TestCourriel {


	//on initialise les objets et variables statiques
	
	private Courriel CourrielConforme;
	private Courriel CourrielAdresseIncorrecte;
	private Courriel CourrielTitreVide;
	private Courriel CourrielCorpsMotsManquants;
	private Courriel CourrielPasDePiecesJointes;
	
	private boolean alreadyInit = false;
	
	@BeforeEach
	public void initAll() {
		
		if(alreadyInit) return;
		
		else {
			
			
			alreadyInit = true;
		}
	}
	

//=======================================================	

	//test sur un courrier classique conforme
	
	@Test
	public void testCourrielConforme() {
		
		CourrielConforme = new Courriel("test@etu.umontpellier.fr", "le titre", "ceci est le corps avec le mot PJ");
		CourrielConforme.ajouterPieceJointe("pieceJointe1");
		
		assertThrows(CourrierInvalideException.class, () -> {
			
			CourrielConforme.envoyer();
		});
	}
	
	
//=======================================================
	
	//test parametré pour l'adresse V1
	//recoit des adresses inscrites manuellements
	
	
	@ParameterizedTest
	@ValueSource(strings = { "34montpellier@23.ui", "hello@gmail.com", "@hotmail.com" })
	public void testCourrielAdresseIncorrecteV2(String adresseTempo) {
		
		CourrielAdresseIncorrecte = new Courriel(adresseTempo, "le titre", "ceci est le corps avec le mot joint");
		CourrielAdresseIncorrecte.ajouterPieceJointe("pieceJointe1");
		
		assertThrows(CourrierInvalideException.class, () -> {
			
			CourrielAdresseIncorrecte.envoyer();
		});
	}
	
	
	//test parametré pour l'adresse V2
	//elle recoit des adresses venant de la fonction addressGenerator
	
	
	@ParameterizedTest
	@MethodSource("adressGenerator")
	public void testCourrielAdresseIncorrecteV1(String adresseTempo) {
		
		CourrielAdresseIncorrecte = new Courriel(adresseTempo, "le titre", "ceci est le corps avec le mot joint");
		CourrielAdresseIncorrecte.ajouterPieceJointe("pieceJointe1");
		
		assertThrows(CourrierInvalideException.class, () -> {
			
			CourrielAdresseIncorrecte.envoyer();
		});
	}
	
	
	//renvoie un flux aléatoire contenant un nombre fixé d'adresses volontairements fausses	
	
	public static Stream<String> addressGenerator() {
		
		ArrayList<String> addressContainer = new ArrayList<String>();
		
		int nbrAdressesTest = 10;
		
		//on va generer tant d'addresse et les ajouter a l'array
		for(int i = nbrAdressesTest; i > 0; i--) {
			
			//generation alea [0, 4] pour generer 5 types d'addresses invalides
			int alea = (int)Math.random() * (4 - 0 + 1) + 0;
			
			switch(alea) {
			
			case 0:
				
				addressContainer.add("@etu.umontpellier.fr");
				break;
				
			case 1:
				
				addressContainer.add("testEtu.umontpellier.fr");
				break;
				
			case 2:
				
				addressContainer.add("test@564.umontpellier.fr");
				break;
				
			case 3:
				
				addressContainer.add("test@etu.umontpellierFr");
				break;
				
			case 4:
				
				addressContainer.add("test@etu.umontpellier.");
				break;
			}	
		}
		
		return addressContainer.stream();
	}
	
	
//=======================================================
	
	//test si le courrier n'a pas de titre
	
	@Test
	public void testCourrielTitreVide() {
		
		CourrielTitreVide = new Courriel("test@etu.umontpellier.fr", "", "ceci est le texte avec le mot jointe");
		CourrielTitreVide.ajouterPieceJointe("pieceJointe1");
		
		assertThrows(CourrierInvalideException.class, () -> {
			
			CourrielTitreVide.envoyer();
		});
	}
	
//=======================================================
	
	//test si le courrier n'a pas les mots demandé dans son corps de texte
	
	@Test
	public void testCourrielCorpsMotsManquants() {
		
		CourrielCorpsMotsManquants = new Courriel("test@etu.umontpellier.fr", "le titre", "ceci est le texte sans les mots demandés");
		CourrielCorpsMotsManquants.ajouterPieceJointe("pieceJointe1");
		
		assertThrows(CourrierInvalideException.class, () -> {
			
			CourrielCorpsMotsManquants.envoyer();
		});
	}
	

//=======================================================
	
	//test si le courrier n'a pas de pieces jointes
	
	@Test
	public void testCourrielPasDePiecesJointes() {
		
		CourrielPasDePiecesJointes = new Courriel("test@etu.umontpellier.fr", "le titre", "ceci est le texte avec le mot PJ");
		
		assertThrows(CourrierInvalideException.class, () -> {
			
			CourrielPasDePiecesJointes.envoyer();
		});
	}


}
